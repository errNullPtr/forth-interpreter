{-# LANGUAGE  TemplateHaskell #-}

module Forth where

import qualified Data.Map.Lazy as Map

import Control.Monad.State
import Control.Monad.Except

import Text.Read (readMaybe)

import Text.Parsec

import Control.Lens hiding (noneOf)

--Start Maintance Code------------------------------------------------------------------------------------------

data Exp = Cmt String | Dump | Num Integer |Plus | Min | Mul | Div | Mod | Dup | Swap | Drop | PP
         | LoopIndex Int | In | Out String | CR | Eq | Lt | Gt
         | Word String [Exp] | Call String | If [Exp] [Exp] | DoLoop [Exp] Bool | Leave
         deriving (Show)
data Forth = Forth {
                    _stack :: [Integer],
                    _loopStack :: [Integer],
                    _heap :: Map.Map String [Exp]
                    } deriving (Show)
makeLenses ''Forth

type ForthS = ExceptT String (StateT Forth IO) --Add error handling to (StateT Forth IO)

emptyForth :: Forth
emptyForth = Forth [] [] Map.empty

pushStack :: Integer -> ForthS ()
pushStack n = stack %= (n:)

popStack :: ForthS Integer
popStack = do
            s <- use stack
            case s of
                [] -> throwError "Empty stack. Can't pop!"
                (n:ns) -> do
                            stack .= ns
                            return n

dumpStack :: ForthS [Integer]
dumpStack = zoom stack get
--

pushLoopStack :: Integer -> ForthS ()
pushLoopStack n = loopStack %= (n:)

popLoopStack :: ForthS Integer
popLoopStack = do
            s <- use loopStack
            case s of
                [] -> throwError "Empty loop stack. Can't pop!"
                (n:ns) -> do
                            loopStack .= ns
                            return n

peekLoopStack :: Int -> ForthS Integer
peekLoopStack i = do
            a <- preuse $ loopStack.ix i
            case a of
                Just n -> return n
                Nothing -> throwError "Loop stack is empty."


setWord :: String -> [Exp] -> ForthS ()
setWord word val = heap %= Map.insert word val

getWord :: String -> ForthS [Exp]
getWord word = do
                h <- use heap
                case Map.lookup word h of
                    Just a -> return a
                    Nothing -> throwError $ "Word \"" ++ word ++ "\" is not defined."
--End Maintance Code--------------------------------------------------------------------------------------------

execProg :: [Exp] -> IO ()
execProg xs = do
    (success, machine) <- runProg xs
    case success of
        Left m  -> putStrLn $ "\nFailed: " ++ m ++ show machine
        Right _ -> putStrLn $ "\nOk: " ++ show machine

runProg :: [Exp] -> IO (Either String (), Forth)
runProg xs = runEval (mapM_ eval xs)

runEval :: Eval -> IO (Either String (), Forth)
runEval ev = runStateT (runExceptT ev) emptyForth

type Eval = ForthS ()

eval :: Exp -> Eval
eval (Cmt _) = return ()
eval Dump = do
    ns <- dumpStack
    liftIO $ print ns
eval (Num n) = pushStack n
eval Plus = binOp (+)
eval Min = binOp (-)
eval Mul = binOp (*)
eval Div = binOp div
eval Mod = binOp mod
eval Dup = do
    a <- popStack
    pushStack a
    pushStack a
eval Swap = do
    a <- popStack
    b <- popStack
    pushStack a
    pushStack b
eval Drop = do
    _ <- popStack
    return ()
eval PP = do
    a <- popStack
    liftIO $ print a
eval (LoopIndex i) = do
    n <- peekLoopStack i
    pushStack n
eval In = do
    s <- liftIO getLine
    case readMaybe s of
        Just n -> pushStack n
        Nothing -> throwError $ "Input \"" ++ s ++ "\" is not a number."
eval (Out s) = liftIO $ putStr s
eval CR = liftIO $ putStrLn ""
eval Eq = binOp (\a b -> if b==a then 1 else 0)
eval Lt = binOp (\a b -> if b<a then 1 else 0)
eval Gt = binOp (\a b -> if b>a then 1 else 0)
eval (Word key val) = setWord key val
eval (Call key) = do
    val <- getWord key
    mapM_ eval val
eval (If yes no) = do
    a <- popStack
    mapM_ eval (if a/=0 then yes else no)
eval Leave = mzero
eval (DoLoop xs plusLoop) = do
    idx <- popStack
    lim <- popStack
    pushLoopStack lim
    pushLoopStack idx
    let go lim' idx' = do
        mapM_ eval xs
        inc <- if plusLoop then popStack else return 1
        -- All you're really checking is that both idx and idx' are on the same side of lim.
        guard $ not $ if inc >= 0 then (lim' < idx') /= (lim' <= idx' + inc) else (lim' <= idx') /= (lim' < idx' + inc)
        _ <- popLoopStack
        _ <- popLoopStack
        pushLoopStack lim'
        pushLoopStack (idx' + inc)
        go lim' (idx' + inc)
    let finish = do
        _ <- popLoopStack
        _ <- popLoopStack
        return ()
    mplus (go lim idx) finish

binOp :: (Integer -> Integer -> Integer) -> ExceptT String (StateT Forth IO) ()
binOp f = do
    a <- popStack
    b <- popStack
    pushStack (b `f` a)

execParse :: String -> IO ()
execParse = execProg . parseForth

parseForth :: String -> [Exp]
parseForth s = case parse forthParser "Carpe Diem" s of
    Left e -> [Out $ "ERROR PARSED: " ++ show e]
    Right xs -> xs

forthParser :: ParsecT String u Identity [Exp]
forthParser = between ws eof $ many forthExp


forthExp :: ParsecT String u Identity Exp
forthExp = foldl1 (<||>) $ map (<* (ws1 <|> eof))[
    Cmt         <$> comments ,
    Num         <$> integer, -- negative numbers suck
    Min         <$  char '-',
    Plus        <$  char '+' ,
    Mul         <$  char '*' ,
    Div         <$  char '/' ,
    PP          <$  char '.' ,
    Eq          <$  char '=' ,
    Lt          <$  char '<' ,
    Gt          <$  char '>' ,
    Mod         <$  char '%' ,
    LoopIndex 0 <$  char 'i' ,
    LoopIndex 2 <$  char 'j' ,
    LoopIndex 4 <$  char 'k' ,
    Num 0       <$  string "false" ,
    Num 1       <$  string "true"  ,
    CR          <$  string "CR"    ,
    Dump        <$  string "dump",
    Dup         <$  string "dup"  ,
    Swap        <$  string "swap",
    Drop        <$  string "drop",
    In          <$  string "in"    ,
    Leave       <$  string "leave" ,
    ifThenElse,
    doLoop,
    Out         <$> stringLike ,
    definition,
    Call        <$> many1 alphaNum  -- if it isn't a keyword, it must be a call
    ]

(<||>) :: ParsecT s u m a -> ParsecT s u m a -> ParsecT s u m a
p <||> q = try p <|> q

ws :: ParsecT String u Identity ()
ws = skipMany (oneOf " \n\r")

ws1 :: ParsecT String u Identity ()
ws1 = skipMany1 (oneOf " \n\r")

lexeme :: ParsecT String u Identity a -> ParsecT String u Identity a
lexeme = between ws ws

integer :: ParsecT String u Identity Integer
integer = rd <$> (plus <|> minus <|> number)
    where rd     = read :: String -> Integer
          plus   = char '+' *> number
          minus  = (:) <$> char '-' <*> number
          number = many1 digit

stringLike :: ParsecT String u Identity [Char]
stringLike = between (char '\'' ) ( char '\'') $ many (noneOf ['\''])

comments :: ParsecT String u Identity [Char]
comments = between (char '(' ) (char ')') $ many (noneOf [')' , '\r', '\n'])

definition :: ParsecT String u Identity Exp
definition = do
    _ <- char ':'
    name <- lexeme (many1 alphaNum)
    prog <- option [] $ manyTill forthExp (try $ lookAhead $ string ";")
    _ <- char ';'
    return $ Word name prog

ifThenElse :: ParsecT String u Identity Exp
ifThenElse = do
    _ <- string "if"
    ws
    yes <- manyTill forthExp (try $ lookAhead $ string "else" <|> string "then")
    no <- option [] $ do
                    _ <- string "else"
                    ws
                    manyTill forthExp (try $ lookAhead $ string "then")
    _ <- string "then"
    return $ If yes no

doLoop :: ParsecT String u Identity Exp
doLoop = do
    _ <- string "do"
    ws
    body <- manyTill forthExp (try $ lookAhead $ string "loop" <|> string "+loop")
    choice [DoLoop body False <$ string "loop", DoLoop body True <$ string "+loop"]



----------------------------------------------------------------------------------------------------------------

--Test Code

example :: String
example = ": neg (n -- n') 0 swap - ; 'enter a number to negate' CR in neg . 'looping' CR 0 50 do i . -5 +loop true if 'always print this' else 'never print this' then dump"

main :: IO ()
main = do
    s <- getContents
    execParse s

--CHANGES

--Updated to Control.Monad.Except, Control.Monad.Error is deprecated
--Removed import Control.Lens.Operators, redunant.
--Removed import import Control.Applicative hiding ((<|>), optional, many), redunant.
--Removed YOLO, added Carpe Diem
--Removed import Safe(atMay)
--Removed redunat brackets
--Move "makeLenses ''Forth" to below Forth definition
--(^=?)  was only used once so I made it expicit on line 71
--Changed "lim idx" to "lim' idx'" on line 160 to avoid possible confusion in scope
--Line ,248,251255,259,262,266,  add explict discard _<-
--Removed old code comments
--Added type signature to binOp
--Added type signature to forthParser
--Added type signature to forthExp
--Added type signature to (<||>)
--Added type signature to ws
--Added type signature to ws1
--Added type signature to lexeme
--Added type signature to integer
--Added type signature to stringLike
--Added type signature to comments
--Added type signature to definition
--Added type signature to ifThenElse
--Added type signature to doLoop
